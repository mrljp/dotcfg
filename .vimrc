" .vimrc
" Author: Larry Pollack <lpgit@pollackworld.us>
" Source: https://bitbucket.org/mrljp/dotcfg/src
" Based on: https://bitbucket.org/durdn/cfg/src

" Preamble with Pathogen   "{{{

execute pathogen#infect()
"}}}
" Basic settings and variables"{{{

filetype plugin indent on
syntax on
set encoding=utf-8 fileencodings=ucs-bom,utf-8,default,latin1
set visualbell noerrorbells " don't beep
set hlsearch incsearch      " hightlight search and incremental search
" set gdefault                " global replace by default
set nowrap                  " not wrap lines
set nu                      " show line numbers
set foldlevel=1             " default foldlevel 1 to see headings
set nobackup noswapfile     " stop backup and swap files
set nocompatible ignorecase smartcase
set autoindent nocindent nosmartindent indentexpr= "setup autoindents
set tabstop=2 shiftwidth=2 backspace=2 expandtab smarttab "setup default tab/shift/expand
set showmode showcmd ttyfast
set guioptions=a            " hide scrollbars/menu/tabs
let mapleader = ","
let maplocalleader = ";"
set foldmethod=marker       " sets the fold method to {{{ }}} markers
set shortmess=atI           " disable welcome screen
set listchars=tab:\|\ ,trail:·,eol:¬
set nospell                 " disable spellcheck for code
colorscheme desert
" End Basic settings and variables}}}
