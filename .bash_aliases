# Alias for debug hexdump format output
alias ox='od -A x -t x1z -v'
# Removes https proxy so curl will work
alias curlproxy='export https_proxy=$http_proxy; export HTTPS_PROXY=$HTTP_PROXY'
# show the current environment variables for proxies
alias showproxy='printenv | sort | grep -i proxy'
# Extract the contents of a RPM file - tar works fine
# alias rpmextract='rpm2cpio $1 | cpio -i -d'
alias rpmextract='tar $1'
# Extract the contents of a DEB file
alias debextract='dpkg-deb -x $1 $2'
