# .bashrc

# If not running interactively, don't do anything
# Do we care if the .bash_profile is running interactively?
case $- in
    *i*) ;;
    *) return;;
esac

#####
# Basic Shell Operation
#####

# don't put duplicate lines or lines starting with space in the history
HISTCONTROL=ignoreboth

# set history length
HISTSIZE=1000
HISTFILESIZE=2000

# append to the history file, don't overwrite it
shopt -s histappend

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS
shopt -s checkwinsize

# set the pattern "**" in a pathname expansion to match all
# files and zero or more directories and subdirectories
# shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

#####
# Prompt
#####

# set a color prompt on color terms
case "$TERM" in
    xterm-*color) color_prompt=yes;;
esac

# uncomment for force a color prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
    xterm*|rxvt*)
        PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
        ;;
    *)
        ;;
esac

#####
# Color support for basic commands
####

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'

    # Set LS colors
    export CLICOLOR=1
    export LSCOLORS=ExFxBxDxCxegedabagacad
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    alias more=less
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

#####
# Basic PATH settings
#####

# Make sure /usr/local/bin comes before /usr/bin
export PATH=/usr/local/bin:$PATH


# Generic settings by O/S

case `uname -s` in
    # macOS
    Darwin)
        # Color settings are different for macOS; no /usr/bin/dircolor
        # Assume color terminal is available; no ls or grep alias is needed, only need to set CLICOLOR
        export CLICOLOR=1
        export LSCOLORS=ExFxBxDxCxegedabagacad

        # Add MySQL
        export PATH=/usr/local/mysql/bin:$PATH

        # Language setup (for ruby/rails)
        export LANGUAGE=en_US.UTF-8
        export LANG=en_US.UTF-8
        export LC_ALL=en_US.UTF-8

        # Load RVM into a shell session *as a function*
        [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

        # Add RVM to PATH for scripting
        PATH=$PATH:$HOME/.rvm/bin

        # Undo the GEM settings
        unset GEM_HOME
        unset GEM_PATH

        # switching PHP versions
        alias use_php53="brew unlink php54 && brew unlink php55 && brew unlink php53 && brew link php53"
        alias use_php54="brew unlink php53 && brew unlink php55 && brew unlink php54 && brew link php54"
        alias use_php55="brew unlink php53 && brew unlink php54 && brew unlink php55 && brew link php55"
        ;;

    Linux)
        # Add Haskell dev tools to PATH
        export PATH=~/.cabal/bin:/opt/cabal/1.20/bin:/opt/ghc/7.8.4/bin:/opt/happy/1.19.4/bin:/opt/alex/3.1.3/bin:.cabal-sandbox/bin:$PATH

        # # Add HMST bin to PATH
        # export PATH=$PATH:/fpc/hmst/smart-device-sw/tools/bin
        # alias gohmst='cd /fpc/hmst/smart-device-sw'
        # eval "$(stack --bash-completion-script "$(which stack)")"

        # # Add ASCIIDoc command used by HMST
        # alias adochmst='stack exec -- asciidoctor -r asciidoctor-diagram -b html5 -a device-name=HMST -a eqnums -a icons=font -a numbered -a revnumber=0 -a revdate=2015-01-01 -a sectids! -a sectanchors -a source-highlighter=pygments -a stem=latexmath'

        # Docker aliases
        alias diclean='docker images | grep '\''<none>'\'' | grep -P '\''[1234567890abcdef]{12}'\'' -o | xargs -L1 docker rmi'

        alias dclean='docker ps -a | grep -v '\''CONTAINER\|_config\|_data\|_run'\'' | cut -c-12 | xargs docker rm'
        ;;

    *)
        ;;
esac

#####
# Common development settings
#####

# Set Python environment
# export PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.7/dist-packages/django
# source virtualenvwrapper.sh
# Alway run pip in a venv
export PIP_REQUIRE_VIRTUALENV=true

#####
# Include settings for various systems and environments
#
# Add new .bash_* files to list to include configurations for specific applications or environments
#####

BASH_INCLUDES="\
  .bash_git \
  .bash_haskell \
  .bash_intel \
"

for b in ${BASH_INCLUDES}; do
    [ -f ~/${b} ] && source ~/${b};
done

